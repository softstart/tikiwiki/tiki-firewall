# Tiki Firewall

Allows to secure the access to Tiki

* Allows you to define what resources can be accessed by any guest/anonymous users
* Requires 2FA (Link by email) to allow access to other resources of tiki (eg. Admin interface, login page, etc.)
